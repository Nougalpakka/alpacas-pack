# Changelog

## **0.1**
**Release date : 2024-11-20**
This version brings many modifications on crafting recipes and even adds new ones for some resources that don't have already one which desserve to get one thanks to their usefulness.

-----

### Additions

#### Convenient Crafting

- Recipe of the **Dispenser** now takes a **String** instead of a **Bow**

> Using a String instead of using a Bow in the craft of the Dispenser, allows players who need many of them to bulk craft them instead of craft them one by one because of the non-stackability of Bows.

- Recipe of the **Netherite Ingot** now takes a single **Gold Ingot** instead of 4

> As Netherite Ingot craft give a signle ingot, it looks more natural to also use a single Gold Ingot in the recipe. At that state of the game, gold ingots should not be an issue to have an impact on the complexity of the recipe.

- Recipe of the **Sticky Piston** can now be made using a **Slimeball** or a **Honey Bottle**

> Sticky Piston can push and pull Slime Block or Honey Block in a similar way, following that logic the Sticky Piston craft should also apply this similarity by being able to craft it using Honey.

-----

#### Corals Extra Craft

- Added **Coral Block** recipes. They can be crafted using **9 Corals and/or Coral Fans** of the same variant

> In the vanilla game, Coral Block have been deliberately made unobtainable to point out the reality of coral reef destruction. That's fine but in a sandbox game, we sometimes just want to have fun without caring for real world problems.

-----

#### Enderman Grief

- Disable pickup ability of Endermen for blocks but they can still pickup plants and mushrooms.

-----

#### Expensive Smithing Template

- Recipes of every Smithing Template duplication now use **1 Diamond Block** instead of 7 Diamonds and needs **7 duping Ingredients** instead of 1

> The way Smithing Template looks make it more logical to have the diamonds in the middle of the recipe and 'duping ingredients' all around. Going for 7 to 9 diamonds for a complete duplication make it much more expensive but that make the duplication much cooler when using a whole Diamond Block, as marking the achievement of getting 9 diamonds.

-----

#### Horse Extra Craft

- Added **Iron Horse Armor** recipe. Can be crafted using **6 Iron Ingots** and **1 Wool** of any color

- Added **Golden Horse Armor** recipe. Can be crafted using **6 Gold Ingots** and **1 Wool** of any color

- Added **Diamond Horse Armor** recipe. Can be crafted using **6 Diamonds** and **1 Wool** of any color

- Changed **Lether Horse Armor** recipe. Can now be crafted using **6 Leathers** and **1 Wool** of any color

- Added **Saddle** recipe. Can be crafted using **5 Leathers** and **3 Iron Ingots** 

-----

#### Irons Craft Fix

- Recipe of the **Iron Trapdoor** now takes **6 Iron Ingots** instead of 4, and gives **2** trapdoors instead of 1

- Recipe of the **Iron Bar** now takes **4 Iron Ingots** and **2 Iron Nuggets** instead of 6 Iron Ingots, and gives **12** Iron Bars instead of 16

> As the vanilla Iron Bar craft as being associated to Iron Trapdoor in this pack, the Iron Bar craft changes to match the look of fence recipes.

- Recipe of the **Iron Door** now gives **1** door instead of 3

> Door crafts has been changed to follow the equivalance logic of Trapdoor Craft Fix pack, where 6 ingredients give either 3 doors (2-block tall, covering up to 6 blocks), or 6 trapdoors (1-block wide, also covering up to 6 blocks). In Irons Craft Fix, Iron Trapdoor are crafted by 2 for 6 Iron Ingots, then the 6 same Iron Ingots should give 1 Iron Door.

- Recipe of **Chain** now give **2** Chains instead of 1

-----

#### Nether Extra Craft

- Added **Glowstone Dust** recipe. It can be crafted using **1 Blaze Powder** and **1 Redstone Dust** shapelessly in the crafting grid, to get **3 Glowstone Dust**

- Added **Quartz** recipe. It can be crafted using **1 Blaze Powder** and **1 Flint** shapelessly in the crafting grid, to get **3 Quartz**

> Glowstone and Quartz can be more and more difficult to get when many players are exploiting Nether resources in large quantity. If both of these can be farmed respectively with a Witch farm and with a Bartering Farm, a solution to get small quantities of those resources should be welcomed.

-----

#### Stairs Craft Fix

- Recipes of **Stairs** now give **6** stairs instead of 4

> This change aims to set on equal footing stone-made stairs that can be crafted in a stonecutter at a 1:1 ratio, and non-stone-made stairs, like wooden stairs that couldn't and must be crafted in a Crafting Table at a 3:2 ratio.\
That quality of life modification guarantee to get a 1:1 ratio either in a Crafting Table or in a Stonecutter for every stairs.

-----

#### Trapdoors Craft Fix

- Recipes of **Trapdoors** now give **6** stairs instead of 2

> This change is made to make trapdoors less expensive by following the logic 
of door crafting recipes. Doors are crafted following a 2:1 ratio, as doors are 
2-block tall. Trapdoors, being 1-block wide should have a 1:1 ratio.

-----

#### Useful Extra Craft

- Added **Bell** recipe. It can be crafted using **5 Gold Ingots**, **1 Gold Nugget** and **1 Stick**

- Added **Cobweb** recipe. It can be crafted using **5 Strings** to get **2** cobwebs

- Added **Name Tag** recipe. It can be crafted using **1 String** and **1 Paper**

- Added **Enchanted Golden Apple** recipe. It can be crafted using **8 Gold Blocks** and **1 Apple**

-----

#### Wandering Trader Notify

- When a Wandering Trader spawns in the world, all players are notified by a **sound** and with a **message** sent in the chat. The Wandering Trader also **glows** for 45 seconds after being notified to players.

> On server with several players, Wandering Trader might be difficult to meet. This little notification feature may help players to find more Wandering Trader in this case.

-----
-----