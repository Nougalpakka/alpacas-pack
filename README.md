# Alpaca's Pack

A set of Minecraft datapacks that aim to bring freshness to the vanilla Minecraft gameplay without breaking the initial feels of the original game.

## Version

*Currently under development* \
version 0.1

## Author

This datapack is made by Nougalpakka.

## Content

### **Convenient Crafting**
*Version : 1.0*

Convenient Crafting modify crafting recipes that are more accessible without giving away the complexity of the vanilla recipes.

### **Corals Extra Craft**
*Version : 1.0*

Corals Extra Craft add 5 new recipes to craft the five variants of Coral Blocks from Corals and Corals Fans.

### **Enderman Grief**
*Version : 1.0*

Enderman Grief disable Enderman ability to pickup blocks from the world. However, they can still pickup flowers, mushrooms and Nether vines.

### **Expensive Smithing Template**
*Version : 1.0*

Expensive Smithing Template changes recipes of smithing template duplication to make it more accurate to its visual while making the recipe a bit more expensive.

### **Horse Extra Craft**
*Version : 1.0*

Horse Extra Craft add crafting recipes for horse-related items like Horse Armor and Saddle.

### **Irons Craft Fix**
*Version : 1.0*

Irons Craft Fix changes crafting recipes for some iron-related items to match other recipes from vanilla or from other packs.

### **Nether Extra Craft**
*Version : 1.0*

Nether Extra Craft add 2 new crafting recipes for Nether resources.

### **Stairs Craft Fix**
*Version : 1.0*

Stairs Craft Fix changes all stair crafting recipes to give 6 stairs, matching the stonecutter recipes.

### **Trapdoors Craft Fix**
*Version : 1.0*

Trapdoors Craft Fix changes all trapdoor crafting recipes to give 6 trapdoors, matching door crafting recipes logic.

### **Useful Extra Craft**
*Version : 1.0*

Useful Extra Craft add 4 crafting recipes for useful items.

### **Wandering Trader Notify**
*Version : 1.0*

Wandering Trader Notify send a notification to all players when a Wandering Trader spawn in the world.

-----