# notify
# Called by: on_update
# As: @e[type=minecraft:wandering_trader,tag=!WTN.notified]
#     A Wandering Trader that doesn't have the tag WTN.notified


# Tag the Wandering Trader
tag @s add WTN.notified

# Get the Wandering Trader wander_target information
execute store result score PosX WTN.trader_info run data get entity @s wander_target[0]
execute store result score PosZ WTN.trader_info run data get entity @s wander_target[2]

# Notify all player in chat, by a sound and by making the Wandering Trader glow
tellraw @a ["",{"text": "A ", "color": "white"}, {"text": "Wandering Trader", "bold": true, "color": "blue"}, {"text": " is heading towards ", "color":"white"}, {"score": {"name": "PosX", "objective": "WTN.trader_info"}, "bold": true, "color": "blue"}, {"text": ", ", "bold": true, "color": "blue"}, {"score": {"name": "PosZ", "objective": "WTN.trader_info"}, "bold": true, "color": "blue"}, {"text": "\n"}, {"selector": "@p", "color": "yellow", "bold": true}, {"text": " is nearby", "color": "white", "bold": false}]
effect give @s minecraft:glowing 45 0 true
execute as @s at @a run playsound minecraft:entity.wandering_trader.reappeared neutral @a