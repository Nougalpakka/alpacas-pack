# Create scoreboard
scoreboard objectives add WTN.trader_info dummy
scoreboard players set PosX WTN.trader_info 0
scoreboard players set PosY WTN.trader_info 0
scoreboard players set PosZ WTN.trader_info 0


# Schedule update loop
schedule function wandering-trader-notify:on_update 5s