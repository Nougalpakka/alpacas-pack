# If a Wandering Trader exist and hasn't been notified yet, run the 'notify' function
execute as @e[type=minecraft:wandering_trader,tag=!WTN.notified] run function wandering-trader-notify:notify


# Reschedule update loop
schedule function wandering-trader-notify:on_update 5s